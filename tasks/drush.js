/*
 * grunt-drush
 * 
 *
 * Copyright (c) 2015 Bas van der Heijden
 * Licensed under the MIT license.
 */

'use strict';

var child_process = require('child_process');

module.exports = function(grunt) {
  grunt.registerTask('drush', 'Issue drush commands through grunt', function(cmd) {
    var options = this.options();
    var done = this.async();
    cmd = cmd || 'status';

    var args = options.arguments || [];

    if (!options.bin) {
        options.bin = process.cwd() + '/vendor/bin/drush';
    }

    grunt.util.spawn({
      cmd: options.bin,
      args: args.concat(cmd.split(' ')),
    }, function(err, res, code) {
      var args = [];
      var func = (code === 0) ? 'ok' : 'error';

      if (res.stdout.length) {
        args.push(res.stdout);
      }
      if (res.stderr.length) {
        args.push(res.stderr);
      }

      grunt.log[func](args);
      done();
    });
  });

  // Slight hack to allow drush commands via grunt
  var drushTried = false;
  grunt.task.run = function() {
    // Parse arguments into an array, returning an array of task+args objects.
    var things = this.parseArgs(arguments).map(this._taskPlusArgs, this);
    // Throw an exception if any tasks weren't found.
    var fails = things.filter(function(thing) { return !thing.task; });
    if (fails.length > 0 && !drushTried) {
      drushTried = true;
      // Check if we can do a drush command with this.
      var cmd = process.argv.slice(2).join(' ');
      grunt.task.run('drush:' + cmd);
      return this;
    }
    else if (fails.length > 0) {
      return this;
    }
    // Append things to queue in the correct spot.
    this._push(things);
    // Make chainable!
    return this;
  };
};
