var path = require('path');
var fs = require('fs');

module.exports = function(grunt) {
  var PREFIXES = ['', 'grunt-', 'grunt-contrib-'];
  var pluginsRoot = path.resolve('node_modules');

  return {
    hasOwnProperty: function(taskName) {
      if (typeof this[taskName] === 'undefined') {
        var found = false;
        var dashedName = taskName.replace(/([A-Z])/g, '-$1').replace(/_+/g, '-').toLowerCase();

        for (var p = PREFIXES.length; p--;) {
          pluginName = PREFIXES[p] + dashedName;
          taskPath = path.join(pluginsRoot, pluginName, 'tasks');
          if (fs.existsSync(taskPath)) {
            found = true;
            break;
          }
        }

        if (found) {
          return false;
        }

        var normalizedTask = taskName;
        var parts = process.argv.slice(2);
        if (parts.length > 1) {
          normalizedTask = parts.join(' ');
          grunt.task._queue = [];
        }

        this[taskName] = 'grunt-drush';
        grunt.task.registerTask(taskName, 'drush:' + normalizedTask);
      }

      return true;
    }
  };
};
